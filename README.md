##### Тестовое задание в Aviasales.ru на позицию Frontend Developer

[Demo](https://eq4l.gitlab.io/aviasales-test-task/) <br>
[Task](https://gitlab.com/eQ4L/aviasales-test-task/-/tree/master/task)

###### Для запуска приложения необходимо:

Желетельно установить `yarn` и node.js версии `10.16.0`

- Склонировать репозиторий
- Установить зависимости `yarn start`
- Запустить приложение `yarn dev`
---------
###### Также можно:
- Прогнать линтер `yarn lint`
- Запустить тесты `yarn test`
- Собрать приложение для продакшена `yarn build`