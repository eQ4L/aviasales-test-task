import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import TicketsLayout from '../index';

describe('TicketsLayout', () => {
  it('Renders correctly', () => {
    const ticketsLayout = shallow(<TicketsLayout />);
    expect(toJson(ticketsLayout)).toMatchSnapshot();
  });
});
