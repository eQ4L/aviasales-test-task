import React, { Fragment } from 'react';
import Header from '../../components/header';
import Filters from '../../components/filters';
import TicketList from '../../containers/ticket-list';

import './style.scss';

const TicketsLayout = () => (
  <Fragment>
    <Header />

    <div className="tickets">
      <div className="tickets__container">
        <Filters />

        <TicketList />
      </div>
    </div>
  </Fragment>
);

export default TicketsLayout;
