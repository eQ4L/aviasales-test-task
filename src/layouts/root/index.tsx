import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import moment from 'moment';
import 'moment/locale/ru';
import rootReducer from '../../reducers';
import Tickets from '../tickets';

const composeEnhancers =
  (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk))
);

moment.locale('ru');

const RootLayout = () => (
  <Provider store={store}>
    <Router basename="/aviasales-test-task">
      <Route exact path="/" component={Tickets} />
    </Router>
  </Provider>
);

export default RootLayout;
