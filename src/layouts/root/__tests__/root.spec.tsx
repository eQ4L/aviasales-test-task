import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import RootLayout from '../index';

describe('RootLayout', () => {
  it('Renders correctly', () => {
    const root = shallow(<RootLayout />);
    expect(toJson(root)).toMatchSnapshot();
  });
});
