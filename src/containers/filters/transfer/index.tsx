import { RootState } from '../../../reducers/types';

import { connect } from 'react-redux';
import TransferFilter from '../../../components/filters/transfer';
import { transferFilters, allItemTransferFilter } from './filters';
import {
  selectFilters,
  selectFetchingStatus,
  setFilters
} from '../../../reducers/tickets';

const mapStateToProps = (state: RootState) => ({
  allItemTransferFilter,
  transferFilters,
  activeFilters: selectFilters(state),
  isFetching: selectFetchingStatus(state)
});

const mapDispatchToProps = {
  setFilters
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TransferFilter);
