import {
  TransferFilter,
  TransferFilters
} from '../../../reducers/tickets/types';

export const transferFilters: TransferFilter[] = [
  {
    type: TransferFilters.NONE,
    label: 'Без пересадок'
  },
  {
    type: TransferFilters.ONE,
    label: '1 пересадка'
  },
  {
    type: TransferFilters.TWO,
    label: '2 пересадки'
  },
  {
    type: TransferFilters.THREE,
    label: '3 пересадки'
  }
];

export const allItemTransferFilter: TransferFilter = {
  type: TransferFilters.ALL,
  label: 'Все'
};
