import { connect } from 'react-redux';
import { fetchTickets } from '../../reducers/tickets';
import Error from '../../components/error';

const mapDispatchToProps = {
  fetchTickets
};

export default connect(
  null,
  mapDispatchToProps
)(Error);
