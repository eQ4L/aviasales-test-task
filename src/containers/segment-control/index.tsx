import { RootState } from '../../reducers/types';

import { connect } from 'react-redux';
import SegmentControl from '../../components/segment-control';
import { segments } from './segments';
import { selectActiveSegment, setSegment } from '../../reducers/tickets';

const mapStateToProps = (state: RootState) => ({
  activeSegment: selectActiveSegment(state),
  segments
});

const mapDispatchToProps = {
  setSegment
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SegmentControl);
