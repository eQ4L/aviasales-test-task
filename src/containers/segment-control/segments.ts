import { Segments, Segment } from '../../reducers/tickets/types';

export const segments: Segment[] = [
  {
    name: 'Самый дешевый',
    type: Segments.CHEAPEST_SEGMENT
  },
  {
    name: 'Самый быстрый',
    type: Segments.FASTEST_SEGMENT
  }
];
