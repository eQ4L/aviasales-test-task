import { RootState } from '../../reducers/types';

import { connect } from 'react-redux';
import TicketList from '../../components/ticket-list';
import {
  fetchTickets,
  ticketsSelector,
  increaseTicketsOffset,
  selectFetchingStatus,
  selectErrorStatus,
  selectTicketsOffset
} from '../../reducers/tickets';

const mapStateToProps = (state: RootState) => ({
  tickets: ticketsSelector(state),
  isFetching: selectFetchingStatus(state),
  isError: selectErrorStatus(state),
  ticketsOffset: selectTicketsOffset(state)
});

const mapDispatchToProps = {
  fetchTickets,
  increaseTicketsOffset
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TicketList);
