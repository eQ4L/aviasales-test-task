import { combineReducers } from 'redux';
import tickets from './tickets';
import { RootState } from './types';

const rootReducer = combineReducers<RootState>({
  tickets
});

export default rootReducer;
