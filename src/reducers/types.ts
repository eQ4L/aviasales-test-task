import { State as TicketsState } from './tickets/types';

export interface RootState {
  tickets: TicketsState;
}

// eslint-disable-next-line @typescript-eslint/class-name-casing
export interface _Action<T> {
  type: string;
  payload?: T;
}

export type Action = _Action<any>;
export type GetState = () => RootState;
export type _Dispatch<T> = (action: T | Promise<T>) => Promise<any>;
export type _ActionCreator<D> = (dispatch: D, getState: GetState) => any;

export type Dispatch = _Dispatch<Action | _ActionCreator<any>>;
export type ActionCreator = _ActionCreator<Dispatch>;
