export interface State {
  fetching: boolean;
  items: Ticket[];
  ticketsOffset: TicketsOffset;
  activeSegment: Segments;
  filters: Filters;
  isError: boolean;
}

export interface Ticket {
  price: number;
  carrier: string;
  segments: TicketSegment[];
}

export interface TicketSegment {
  origin: string;
  destination: string;
  date: Date;
  stops: string[];
  duration: number;
}

export type TicketsOffset = number;

export enum Segments {
  CHEAPEST_SEGMENT = 'CHEAPEST',
  FASTEST_SEGMENT = 'FASTEST'
}

export interface Segment {
  name: string;
  type: Segments;
}

export interface Filters {
  [type: number]: boolean;
}

export enum TransferFilters {
  ALL = 1000,
  NONE = 0,
  ONE = 1,
  TWO = 2,
  THREE = 3
}

export interface TransferFilter {
  type: TransferFilters;
  label: string;
}
