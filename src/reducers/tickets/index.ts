import {
  _Action,
  _Dispatch,
  Action,
  GetState,
  ActionCreator,
  RootState
} from '../types';
import {
  Segments,
  State,
  Ticket,
  TicketsOffset,
  TransferFilters,
  Filters
} from './types';

import { createSelector } from 'reselect';
import { getSearchId, getTickets } from '../../services/ticket';
import FilterService from '../../services/filter';

type ActionTicketsSet = _Action<Ticket[]>;
type ActionSegmentSet = _Action<Segments>;
type ActionFiltersSet = _Action<TransferFilters>;
type ActionEmpty = _Action<void>;

type DispatchTicketsSet = _Dispatch<ActionTicketsSet | ActionEmpty>;

const TICKETS_OFFSET: TicketsOffset = 10;

const rootType = '@app/tickets';
export const TICKETS_FETCHING_STARTED = `${rootType}/fetching/started`;
export const TICKETS_FETCHING_FINISHED = `${rootType}/fetching/finished`;
export const TICKETS_SET = `${rootType}/tickets/set`;
export const TICKETS_OFFSET_INCREASE = `${rootType}/tickets/offset/increase`;
export const TICKETS_ERROR_TOGGLE = `${rootType}/tickets/error/toggle`;
export const TICKETS_SEGMENT_SET = `${rootType}/tickets/segment/set`;
export const TICKETS_FILTERS_SET = `${rootType}/tickets/filters/set`;

const initialState: State = {
  fetching: false,
  items: [],
  ticketsOffset: TICKETS_OFFSET,
  activeSegment: Segments.CHEAPEST_SEGMENT,
  filters: {},
  isError: false
};

export default (state: State = initialState, action: Action) => {
  switch (action.type) {
    case TICKETS_FETCHING_STARTED:
      return {
        ...state,
        fetching: true
      };
    case TICKETS_FETCHING_FINISHED:
      return {
        ...state,
        fetching: false
      };
    case TICKETS_SET:
      if (!action.payload) {
        return state;
      }

      return {
        ...state,
        items: action.payload
      };
    case TICKETS_OFFSET_INCREASE:
      return {
        ...state,
        ticketsOffset: state.ticketsOffset + TICKETS_OFFSET
      };
    case TICKETS_ERROR_TOGGLE:
      return {
        ...state,
        isError: !state.isError
      };
    case TICKETS_SEGMENT_SET:
      return {
        ...state,
        activeSegment: action.payload
      };
    case TICKETS_FILTERS_SET:
      if (action.payload === TransferFilters.ALL) {
        return {
          ...state,
          ticketsOffset: TICKETS_OFFSET,
          filters: {}
        };
      }

      if (state.filters[action.payload]) {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const { [action.payload]: omit, ...rest } = state.filters;

        return {
          ...state,
          ticketsOffset: TICKETS_OFFSET,
          filters: {
            ...rest
          }
        };
      }

      return {
        ...state,
        ticketsOffset: TICKETS_OFFSET,
        filters: {
          ...state.filters,
          [action.payload]: true
        }
      };
    default:
      return state;
  }
};

// Selectors
export const selectFetchingStatus = (state: RootState): boolean =>
  state.tickets.fetching;

export const selectErrorStatus = (state: RootState): boolean =>
  state.tickets.isError;

export const selectActiveSegment = (state: RootState): Segments =>
  state.tickets.activeSegment;

export const selectFilters = (state: RootState): Filters =>
  state.tickets.filters;

export const selectTickets = (state: RootState): Ticket[] =>
  state.tickets.items;

export const selectTicketsOffset = (state: RootState): TicketsOffset =>
  state.tickets.ticketsOffset;

// Reselectors
export const ticketsSelector = createSelector<
  any,
  Ticket[],
  Filters,
  Segments,
  TicketsOffset,
  Ticket[]
>(
  [selectTickets, selectFilters, selectActiveSegment, selectTicketsOffset],
  (tickets, filters, activeSegment, offset) =>
    new FilterService(tickets)
      .filterTickets(filters)
      .sortTickets(activeSegment)
      .selectTicketsByOffset(offset)
);

// Actions
const startingFetching = (): ActionEmpty => ({
  type: TICKETS_FETCHING_STARTED
});

const finishingFetching = (): ActionEmpty => ({
  type: TICKETS_FETCHING_FINISHED
});

const errorAction = (): ActionEmpty => ({
  type: TICKETS_ERROR_TOGGLE
});

const setTickets = (tickets: Ticket[]): ActionTicketsSet => ({
  type: TICKETS_SET,
  payload: tickets
});

export const increaseTicketsOffset = (): ActionEmpty => ({
  type: TICKETS_OFFSET_INCREASE
});

export const setSegment = (segmentType: Segments): ActionSegmentSet => ({
  type: TICKETS_SEGMENT_SET,
  payload: segmentType
});

export const setFilters = (filterType: TransferFilters): ActionFiltersSet => ({
  type: TICKETS_FILTERS_SET,
  payload: filterType
});

// Action creators
export const fetchTickets = (): ActionCreator => async (
  dispatch: DispatchTicketsSet,
  getState: GetState
): Promise<void> => {
  try {
    if (getState().tickets.isError) {
      dispatch(errorAction());
    }

    dispatch(startingFetching());

    const searchId = await getSearchId();
    const data = await getTickets(searchId);

    dispatch(setTickets(data.tickets));
    dispatch(finishingFetching());
  } catch (error) {
    dispatch(errorAction());
    dispatch(finishingFetching());
  }
};
