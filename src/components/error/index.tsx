import { Ticket } from '../../reducers/tickets/types';

import React from 'react';
import error from '../../assets/error.svg';
import './style.scss';

interface Props {
  fetchTickets: () => Ticket[];
}

const Error = ({ fetchTickets }: Props) => (
  <div className="error__container">
    <div className="error__item">
      <img src={error} alt="error-logo" className="error__logo" />
      Возникла ошибка при выполнении запроса
      <button onClick={fetchTickets} className="error__button">
        Повторить запрос
      </button>
    </div>
  </div>
);

export default Error;
