import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Error from '../index';

const props = {
  fetchTickets: jest.fn()
};

describe('ErrorComponent', () => {
  it('Renders correctly', () => {
    const { fetchTickets } = props;
    const component = shallow(<Error fetchTickets={fetchTickets} />);

    expect(toJson(component)).toMatchSnapshot();
  });

  it('Call click callback one time', () => {
    const { fetchTickets } = props;
    const component = shallow(<Error fetchTickets={fetchTickets} />);

    component.find('button').simulate('click');

    expect(fetchTickets).toHaveBeenCalledTimes(1);
  });
});
