import React from 'react';
import './style.scss';

interface Props {
  label: string;
  onChange: () => void;
  isActive: boolean;
}

const CheckboxItem = ({ label, onChange, isActive }: Props) => (
  <div className="checkbox-item">
    <label className="checkbox-item__label">
      <span className="checkbox">
        <input
          className="checkbox__field"
          type="checkbox"
          onChange={onChange}
          checked={isActive}
        />
        <span className="checkbox__face" />
      </span>
      {label}
    </label>
  </div>
);

export default CheckboxItem;
