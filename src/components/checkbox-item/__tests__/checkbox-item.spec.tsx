import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import CheckboxItem from '../index';

const props = {
  label: 'test',
  onChange: jest.fn(),
  isActive: false
};

describe('CheckboxItemComponent', () => {
  it('Renders correctly', () => {
    const { label, onChange, isActive } = props;

    const checkbox = shallow(
      <CheckboxItem label={label} onChange={onChange} isActive={isActive} />
    );
    expect(toJson(checkbox)).toMatchSnapshot();
  });
});
