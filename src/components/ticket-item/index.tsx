import { Ticket } from '../../reducers/tickets/types';

import React from 'react';
import pluralize from 'pluralize-ru';
import { getHumanDuration, getHumanFlightTime } from '../../services/date';
import './style.scss';

interface Props {
  ticket: Ticket;
}

const TicketItem = ({ ticket }: Props) => (
  <div className="ticket">
    <div className="ticket__header">
      <div className="ticket__price">{ticket.price} P</div>
      <div className="ticket__carrier">
        <img
          className="ticket__carrier-img"
          alt="carrier-logo"
          src={`//pics.avs.io/99/36/${ticket.carrier}.png`}
          srcSet={`//pics.avs.io/99/36/${ticket.carrier}@2x.png 2x`}
        />
      </div>
    </div>

    <div className="ticket__body">
      {ticket.segments.map((segment, idx: number) => (
        <div className="ticket__segment" key={idx}>
          <div className="ticket__flight">
            <div className="ticket__flight-label">
              {segment.origin} - {segment.destination}
            </div>
            <div className="ticket__flight-value">
              {getHumanFlightTime(segment.date, segment.duration)}
            </div>
          </div>

          <div className="ticket__flight">
            <div className="ticket__flight-label">В пути</div>
            <div className="ticket__flight-value">
              {getHumanDuration(segment.duration)}
            </div>
          </div>

          <div className="ticket__flight">
            <div className="ticket__flight-label">
              {segment.stops.length ? `${segment.stops.length} ` : null}
              {pluralize(
                segment.stops.length,
                'пересадки',
                'пересадка',
                'пересадки',
                'пересадки'
              )}
            </div>
            <div className="ticket__flight-value">
              {segment.stops.length ? segment.stops.join(', ') : 'Прямой'}
            </div>
          </div>
        </div>
      ))}
    </div>
  </div>
);

export default TicketItem;
