import React, { FC } from 'react';

import './style.scss';

const Loader: FC = () => (
  <div className="loader__container">
    <div className="loader" />
  </div>
);

export default Loader;
