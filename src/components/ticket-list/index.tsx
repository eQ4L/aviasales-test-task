import { Ticket } from '../../reducers/tickets/types';

import React, { PureComponent } from 'react';
import SegmentControl from '../../containers/segment-control';
import Error from '../../containers/error';
import TicketItem from '../ticket-item';
import Button from '../button';
import Loader from '../loader';

interface Props {
  fetchTickets: () => void;
  tickets: Ticket[];
  increaseTicketsOffset: () => void;
  isFetching: boolean;
  isError: boolean;
  ticketsOffset: number;
}

class TicketList extends PureComponent<Props> {
  componentDidMount(): void {
    this.props.fetchTickets();
  }

  render() {
    const {
      tickets,
      increaseTicketsOffset,
      isFetching,
      isError,
      ticketsOffset
    } = this.props;
    const isMoreButtonVisible = !(ticketsOffset > tickets.length);

    if (isFetching) return <Loader />;

    if (isError) return <Error />;

    return (
      <div className="tickets-list">
        <SegmentControl />

        {tickets.map((ticket: Ticket, idx: number) => (
          <TicketItem ticket={ticket} key={idx} />
        ))}

        {isMoreButtonVisible && (
          <Button onClick={increaseTicketsOffset} label={'Показать еще'} />
        )}
      </div>
    );
  }
}

export default TicketList;
