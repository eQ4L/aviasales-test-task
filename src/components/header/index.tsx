import React, { FC } from 'react';

import logo from '../../assets/logo.svg';
import './style.scss';

const Header: FC = () => (
  <header className="header">
    <img src={logo} alt="app=logo" />
  </header>
);

export default Header;
