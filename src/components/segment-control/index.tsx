import { Segments, Segment } from '../../reducers/tickets/types';

import React from 'react';
import cx from 'classnames';
import './style.scss';

interface Props {
  segments: Segment[];
  activeSegment: Segments;
  setSegment: (segmentType: Segments) => void;
}

const SegmentControl = ({
  segments = [],
  activeSegment,
  setSegment
}: Props) => (
  <ul className={cx('segment-control')}>
    {segments.map((segment: Segment, idx: number) => (
      <li
        className={cx('segment-control__tab', {
          'segment-control__tab--active': activeSegment === segment.type
        })}
        onClick={() => setSegment(segment.type)}
        key={idx}
      >
        {segment.name}
      </li>
    ))}
  </ul>
);

export default SegmentControl;
