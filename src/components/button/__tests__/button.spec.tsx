import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Button from '../index';

const props = {
  label: 'test',
  onClick: jest.fn()
};

describe('ButtonComponent', () => {
  it('Renders correctly', () => {
    const { label, onClick } = props;

    const button = shallow(<Button label={label} onClick={onClick} />);
    expect(toJson(button)).toMatchSnapshot();
  });

  it('Call click callback one time', () => {
    const { label, onClick } = props;
    const component = shallow(<Button onClick={onClick} label={label} />);

    component.find('button').simulate('click');

    expect(onClick).toHaveBeenCalledTimes(1);
  });
});
