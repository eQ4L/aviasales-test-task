import React from 'react';
import './style.scss';

interface Props {
  onClick: () => void;
  label: string;
}
const Button = ({ onClick, label }: Props) => (
  <button className="button" onClick={onClick}>
    {label}
  </button>
);

export default Button;
