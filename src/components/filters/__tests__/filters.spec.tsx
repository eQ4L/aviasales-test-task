import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Filters from '../index';

describe('FiltersComponent', () => {
  it('Renders correctly', () => {
    const component = shallow(<Filters />);

    expect(toJson(component)).toMatchSnapshot();
  });
});
