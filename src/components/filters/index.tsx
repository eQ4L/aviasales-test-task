import React, { FC } from 'react';

import TransferFilter from '../../containers/filters/transfer';

const Filters: FC = () => (
  <div className="filters">
    <TransferFilter />
  </div>
);

export default Filters;
