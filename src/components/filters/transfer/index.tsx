import {
  TransferFilters,
  TransferFilter,
  Filters
} from '../../../reducers/tickets/types';

import React from 'react';

import CheckboxItem from '../../checkbox-item';
import Loader from '../../loader';
import './style.scss';

interface Props {
  transferFilters: TransferFilter[];
  setFilters: (filterType: TransferFilters) => void;
  activeFilters: Filters;
  allItemTransferFilter: TransferFilter;
  isFetching: boolean;
}

const TransferItem = ({
  transferFilters,
  allItemTransferFilter,
  activeFilters,
  setFilters,
  isFetching
}: Props) => {
  const isAllFilterActive = () => {
    const activeFiltersLength = Object.keys(activeFilters).length;
    const transferFiltersLength = transferFilters.length;

    return (
      activeFiltersLength === transferFiltersLength || activeFiltersLength === 0
    );
  };

  if (isFetching) return <Loader />;

  return (
    <div className="transfer-filter">
      <div className="transfer-filter__title">Количество пересадок</div>

      <CheckboxItem
        label={allItemTransferFilter.label}
        isActive={isAllFilterActive()}
        onChange={() => setFilters(allItemTransferFilter.type)}
      />

      {transferFilters.map((filter: TransferFilter, idx) => (
        <CheckboxItem
          key={idx}
          label={filter.label}
          isActive={activeFilters[filter.type] || false}
          onChange={() => setFilters(filter.type)}
        />
      ))}
    </div>
  );
};

export default TransferItem;
