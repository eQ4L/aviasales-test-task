import { AxiosResponse } from 'axios';
import { Ticket } from '../reducers/tickets/types';

import http from './http';

interface SearchIdResponse {
  searchId: string;
}

export const getSearchId = async (): Promise<string> => {
  try {
    const response: AxiosResponse<SearchIdResponse> = await http.get('/search');

    return response.data.searchId;
  } catch (error) {
    throw error;
  }
};

interface TicketResponse {
  stop: boolean;
  tickets: Ticket[];
}

export const getTickets = async (searchId: string): Promise<TicketResponse> => {
  try {
    const response: AxiosResponse<TicketResponse> = await http.get('/tickets', {
      params: {
        searchId
      }
    });

    return response.data;
  } catch (error) {
    throw error;
  }
};
