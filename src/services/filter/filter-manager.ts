import { Ticket } from '../../reducers/tickets/types';
import { getTicketDuration } from '../date';

class FilterManager {
  tickets: Ticket[];

  constructor(tickets: Ticket[]) {
    this.tickets = tickets;
  }

  sortByPrice = (): void => {
    this.tickets = this.tickets.sort((a, b) => a.price - b.price);
  };

  sortByDuration = (): void => {
    this.tickets = this.tickets.sort((a, b) =>
      getTicketDuration(a.segments) < getTicketDuration(b.segments) ? -1 : 1
    );
  };
}

export default FilterManager;
