import { Filter } from './types';
import FilterManager from './filter-manager';
import {
  Filters,
  Segments,
  Ticket,
  TicketsOffset
} from '../../reducers/tickets/types';

class FilterService extends FilterManager implements Filter {
  filterTickets = (filters: Filters): FilterService => {
    const filtersList = Object.keys(filters);

    if (!filtersList.length) {
      return this;
    }

    this.tickets = this.tickets.filter(
      ticket =>
        !ticket.segments.some(
          segment => !filtersList.includes(String(segment.stops.length))
        )
    );

    return this;
  };

  sortTickets = (segment: Segments): FilterService => {
    if (segment === Segments.CHEAPEST_SEGMENT) {
      this.sortByPrice();
    } else if (segment === Segments.FASTEST_SEGMENT) {
      this.sortByDuration();
    }

    return this;
  };

  selectTicketsByOffset = (offset: TicketsOffset): Ticket[] =>
    this.tickets.slice(0, offset);
}

export default FilterService;
