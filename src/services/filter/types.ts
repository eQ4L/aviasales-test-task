import FilterService from './';
import {
  Filters,
  Segments,
  Ticket,
  TicketsOffset
} from '../../reducers/tickets/types';

export interface Filter {
  filterTickets: (filters: Filters) => FilterService;
  sortTickets: (segment: Segments) => FilterService;
  selectTicketsByOffset: (offset: TicketsOffset) => Ticket[];
}
