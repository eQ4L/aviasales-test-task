import { TicketSegment } from '../reducers/tickets/types';

import moment from 'moment';

export const getTicketDuration = (segments: TicketSegment[]) => {
  let duration = 0;

  for (let i = 0; i < segments.length; i++) {
    duration += segments[i].duration;
  }

  return duration;
};

export const getHumanDuration = (duration: number): string => {
  const hours = Math.floor(duration / 60);
  const minutes = duration - hours * 60;

  return `${hours}ч ${minutes}м`;
};

export const getHumanFlightTime = (
  startTime: Date,
  endTime: number
): string => {
  const humanStartTime = moment(startTime).format('LT');
  const humanEndTime = moment(startTime)
    .add(endTime, 'minutes')
    .format('LT');

  return `${humanStartTime} - ${humanEndTime}`;
};
